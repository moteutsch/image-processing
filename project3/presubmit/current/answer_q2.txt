In the "band-pass" interpretation of Laplacian pyramid, each level of the
laplacian pyramid corresponds to a band of the original image in fourier
space such that the sum of all the bands will give back the original image.
Now, larger Gaussian filters will give smaller low pass filters in fourier
space, so that, in the Laplacian pyramid, the bands will be larger. This
means that each level of the Laplacian pyramid (of the image) captures more
information.

(I explain this in interpretation also in my answer to question 3.)

Hence, the blend will be **worse** (to a certain extent, obviously) as
increasing the blur size will to some degree cancel out the effect of
pyramid blending versus regular feathering (i.e., pyramid blending with
a single level). I.e., as the size of image blur kernel size goes to infinity,
the pyramid blending will be similar to a single-level pyramid.
