import sys
import os
import time
import math

sys.path.insert(1, os.path.join(sys.path[0], '../project1'))

import src.helper as h
import src.sol1 as sol1
import sol3 as sol3
import numpy as np
import matplotlib.pyplot as plt




#####################




def DFT_change_of_basis_matrix(N):
    """
    Returns a change-of-basis matrix for converting to frequency domain. Depends
    on size of image.
    """
    return np.exp(
        np.dot(np.arange(N).reshape(N, 1), np.arange(N).reshape(N, 1).T)
        * ((-2 * np.pi * 1j) / N)
    )


def IDFT_change_of_basis_matrix(N):
    """
    See DFT_change_of_basis_matrix. (Inverse matrix.)
    """
    return np.linalg.inv(DFT_change_of_basis_matrix(N))


def DFT(signal):
    """
    :param signal (array dtype float32 (N,1))
    :return fourier_signal (array dtype complex128 (N,1))
    """

    N = signal.shape[0]
    return np.dot(DFT_change_of_basis_matrix(N), signal)


def IDFT(fourier_signal):

    N = fourier_signal.shape[0]
    return np.dot(IDFT_change_of_basis_matrix(N), fourier_signal)


def DFT2(image):
    """
    Applies DFT to columns and then to rows (of result).
    """
    return DFT(DFT(image).T).T





def normalize_fourier(fourier):
    """
    Normalization of fourier for visualization as mentioned in "Tips: Fourier display" section
    """
    return np.log(1 + np.abs(fourier))






########################


def create_pyramid_dirs(out_dir):
    try:
        print('Creating directory')
        os.mkdir(out_dir)
    except:
        print('Directory already exists...')
        #print('Cannot save to directory "' + out_dir + '". Already exists.')
        #sys.exit(1)

    print('Creating sub-directories')
    try:
        os.mkdir(out_dir + '/gaussian')
    except:
        pass
    try:
        os.mkdir(out_dir + '/laplacian')
    except:
        pass


def main():
    if len(sys.argv) < 3:
        print('Base usage: python3 save.py <operation> <image_path>')
        sys.exit(1)

    #image_gray = sol1.read_image(sys.argv[2], sol1.REPRESENTATION_GRAYSCALE)

    if sys.argv[1] == 'test':
        print('Okay, it works...')
    elif sys.argv[1] in ['pyramid', 'pyramids']:
        image_gray = sol3.read_image(sys.argv[2], sol1.REPRESENTATION_GRAYSCALE)
        if len(sys.argv) != 6:
            print('Usage: <out_dir> <max_levels> <filter_size>')
            sys.exit(1)

        out_dir, max_levels, filter_size = sys.argv[3:]
        out_dir = sys.argv[3].strip('/')
        max_levels = int(max_levels)
        filter_size = int(filter_size)

        print('Building gaussian pyramid:')
        g_pyramid, g_filter = sol3.build_gaussian_pyramid(
            image_gray, max_levels, filter_size)

        print('Building laplacian pyramid:')
        l_pyramid, l_filter = sol3.build_laplacian_pyramid(
            image_gray, max_levels, filter_size)

        for l in [image_gray] + l_pyramid:
            plt.figure()
            f = DFT2(l)
            print(f.shape, f.dtype, "\n")
            plt.imshow(normalize_fourier(f), cmap = plt.get_cmap('gray'))
            plt.show()


        image_from_laplacian = sol3.laplacian_to_image(
            l_pyramid, l_filter, ([1] * (len(l_pyramid) - 1)) + [0])

        plt.figure()
        plt.imshow(image_gray, cmap = plt.get_cmap('gray'))

        image_from_laplacian2 = sol3.laplacian_to_image(
            l_pyramid, l_filter, [1] + ([0] * (len(l_pyramid) - 1)))

        plt.figure()
        plt.imshow(image_from_laplacian, cmap = plt.get_cmap('gray'))

        plt.figure()
        plt.imshow(image_from_laplacian2, cmap = plt.get_cmap('gray'))

        plt.show()

        image_from_laplacian = sol3.laplacian_to_image(
            l_pyramid, l_filter, [1] * len(l_pyramid))
        image_from_laplacian_changed = sol3.laplacian_to_image(
            l_pyramid, l_filter, [3.0, 0.5] + ([1] * (len(l_pyramid) - 2)))

        print('Filter used: ', g_filter, l_filter)
        print()

        create_pyramid_dirs(out_dir)

        # Write run params
        with open(out_dir + '/run-params', 'w') as f:
            f.write(time.strftime('%c') + '\n')
            f.write(str(sys.argv))

        for i in range(len(g_pyramid)):
            h.imsave(out_dir + '/gaussian/level-' + str(i) + '.jpg', g_pyramid[i])

        for i in range(len(l_pyramid)):
            h.imsave(out_dir + '/laplacian/level-' + str(i) + '.jpg', l_pyramid[i])

        h.imsave(out_dir + '/image-from-laplacian-regular.jpg', image_from_laplacian)
        h.imsave(out_dir + '/image-from-laplacian-weird.jpg',
                 sol3.stretch_image(image_from_laplacian_changed))

        h.imsave(out_dir + '/gaussian-pyramid.jpg', sol3.render_pyramid(g_pyramid, 100))
        h.imsave(out_dir + '/laplacian-pyramid.jpg', sol3.render_pyramid(l_pyramid, 100))

        sol3.display_pyramid(g_pyramid, 100)
        sol3.display_pyramid(l_pyramid, 100)

    elif sys.argv[1] in ['test_laplacian_reconstruction', 'test-laplacian-reconstruction']:
        image_gray = sol3.read_image(sys.argv[2], sol1.REPRESENTATION_GRAYSCALE)
        # if len(sys.argv) != 5:
        #     print('Usage: <max_levels> <filter_size>')
        #     sys.exit(1)

        # max_levels, filter_size = sys.argv[3:]
        # max_levels = int(max_levels)
        # filter_size = int(filter_size)

        success = 0
        failed = 0

        for max_levels in range(2, 8):
            for filter_size in range(3, 31, 2):

                l_pyramid, l_filter = sol3.build_laplacian_pyramid(
                    image_gray, max_levels, filter_size)
                image_from_laplacian = sol3.laplacian_to_image(
                    l_pyramid, l_filter, [1] * len(l_pyramid))

                #print('Maximum difference', max(abs(image_gray -
                # image_from_laplacian)))

                print('Okay')
                print(image_gray.shape[0] * image_gray.shape[1])
                print(np.sum(((image_gray - image_from_laplacian) > (10**-12))))
                are_all_close = np.allclose(image_gray,
                                            image_from_laplacian,
                        rtol=0.0, atol=(10 ** -6))
                if are_all_close:
                    success += 1
                else:
                    failed += 1
                    print('Testing for settings: max_levels=' + str(max_levels)
                          + '; filter_size=' + str(filter_size))
                    print('Not close within 10^-6!')
                    print('Maximum difference', np.argmax(abs(image_gray - image_from_laplacian)))

        print('Total runs: ' + str(success + failed))
        print('Success: ' + str(success))
        print('Failure: ' + str(failed))
    elif sys.argv[1] in ['blending_example', 'blending_examples']:
        if str(sys.argv[2]) == '1':
            sol3.blending_example1()
        elif str(sys.argv[2]) == '2':
            sol3.blending_example2()
        else:
            sol3.blending_example3()
    else:
        print('Invalid command...')


if __name__ == '__main__':
    main()
