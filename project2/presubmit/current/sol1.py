import numpy as np

# For image IO
from scipy.misc import imread, imshow, imsave

# Image plotting
import matplotlib.pyplot as plt

# RGB 2 gray
from skimage.color import rgb2gray

# %matplotlib qt
# %matplotlib inline

# ========== Constants: =======

REPRESENTATION_GRAYSCALE = 1
REPRESENTATION_RGB = 2

REPRESENTATIONS = [REPRESENTATION_GRAYSCALE, REPRESENTATION_RGB]

RGB_TO_YIQ_MATRIX = np.array([
    [0.299, 0.587, 0.114],
    [0.596, -0.275, -0.321],
    [0.212, -0.523, 0.311]
])
YIQ_TO_RGB_MATRIX = np.linalg.inv(RGB_TO_YIQ_MATRIX)

# ==========  Helpers: ========

def _guard_representation(representation):
    if representation not in REPRESENTATIONS:
        raise ValueError

# ========== Image IO: ========

def read_image(filename, representation):
    """
    Reads image from file into "representation" and returns it as image
    np.ndarray of type np.float32 with intensities in [0, 1]
    """

    _guard_representation(representation)
    if not isinstance(filename, str):
        print(str(filename), type(filename))
        raise ValueError

    im = imread(filename)
    if representation == REPRESENTATION_GRAYSCALE:
        return rgb2gray(im)
    return im.astype(np.float32) / 255

def display_image_array(im, representation):
    """
    Display image given by np.ndarray using matplotlib
    """

    _guard_representation(representation)
    if representation == REPRESENTATION_GRAYSCALE:
        plt.imshow(im, cmap = plt.get_cmap('gray'), vmin = 0, vmax = 1)
    else:
        plt.imshow(im, vmin = 0, vmax = 1)

def imdisplay(filename, representation):
    """
    Display an image file using matplotlib, given a representation in which to show it
    """
    display_image_array(read_image(filename, representation), representation)

# ========== Manipulations: ========

def rgb2yiq(im_rgb):
    """
    Convert RGB (x, y, 3) ndarray to YIQ (same shape)
    """
    return np.dot(im_rgb, RGB_TO_YIQ_MATRIX.T)

def yiq2rgb(im_yiq):
    """
    Convert YIQ (x, y, 3) ndarray to RGB (same shape)
    """
    return np.dot(im_yiq, YIQ_TO_RGB_MATRIX.T)

def is_image_grayscale(im):
    return len(im.shape) == 2

# ========== Algorithms: ========

def histogram_equalize(im):
    """
    Perform the histogram equalization algorithm to an image (grayscale or RGB)

    Returns:
        1. Equalized image
        2. Histogram of original image
        3. Histogram of equalized image
    """
    is_input_rgb = not is_image_grayscale(im)
    if is_input_rgb:
        # RGB; switch to YIQ
        im_yiq = rgb2yiq(im)
        im_to_change = im_yiq[:,:,0].copy()
    else:
        im_to_change = im

    # Convert image from [0, 1] representation to [0, 255] (integer)
    im_to_change = (im_to_change * 255)

    # Calculate histogram of integral image, with bins for each possible intensity
    hist, bin_edges = np.histogram(im_to_change, bins=256)

    cumulative_histogram = np.cumsum(hist)

    total_pixels = im_to_change.shape[0] * im_to_change.shape[1]
    normalized_cumulative_histogram = 255 * (cumulative_histogram / total_pixels)


    first_non_zero_gray_level = min(np.nonzero(normalized_cumulative_histogram)[0])
    first_zero_gray_level = min(np.nonzero(normalized_cumulative_histogram)[0])

    stretched_normalized_cumulative_histogram = ((
            (normalized_cumulative_histogram - normalized_cumulative_histogram[first_non_zero_gray_level]) /
            ((normalized_cumulative_histogram[-1] - normalized_cumulative_histogram[first_non_zero_gray_level]))
        ) * 255).round().astype(np.uint8)

    equalized_image = stretched_normalized_cumulative_histogram[im_to_change.round().astype(np.uint8)].astype(np.float32) / 255

    if is_input_rgb:
        new_yiq = im_yiq.copy()
        new_yiq[:,:,0] = equalized_image
        return [yiq2rgb(new_yiq), hist, np.histogram(equalized_image, bins=256)[0]]

    # NOTE: FIXME: Doesn't work in case of RGB because im_yiq[:,:,0] doesn't create a reference to original (view)
    return [equalized_image, hist, np.histogram(equalized_image, bins=256)[0]]

def quantize(im, n_colors, n_iters):
    """
    Perform quantization algorithm on im (color or grayscale), converting it to have
    "n_colors" colors. The algorithm should run for "n_iters" iterations, or until it converges.

    Returns:
        1. Quantized image
        2. An array of the sum-of-squares error for each iteration
    """

    is_input_rgb = not is_image_grayscale(im)
    if is_input_rgb:
        im_yiq = rgb2yiq(im)
        im_2 = im[:,:,0].copy()
    else:
        im_2 = im.copy()

    im_2 = (im_2*255).astype(np.uint8)
    #im_2_flat = im_2.flatten()

    total_hist, edges = np.histogram(im_2, bins=256)
    print(type(total_hist), total_hist.dtype)

    zs = (np.arange(n_colors + 1) * round(255/n_colors)).astype(np.uint8)
    zs[n_colors] = 255
    qs = []

    errors = []
    for i in range(n_iters):
        hist, bin_edges = np.histogram(im_2, zs)
        old_qs = qs
        old_zs = zs
        qs = []
        for j in range(n_colors):
            intensities = total_hist[np.arange(zs[j], zs[j + 1])]
            #intensities = im_2_flat[np.arange(zs[j], zs[j+1])]
            qs.append(round(np.dot(np.arange(zs[j], zs[j+1]), intensities) / sum(intensities)))

        zs = np.array([0] + [ round((qs[i] + qs[i + 1])/2) for i in range(n_colors - 1) ] + [255]).astype(np.uint8)
        errors.append(quantization_error(total_hist, qs, zs))

        if np.array_equal(old_qs, qs) and np.array_equal(old_zs, zs):
            # Convergence! Stop iterating.
            break

    lup = np.empty(256, np.uint8)
    for i in range(n_colors):
        for j in range(zs[i], zs[i + 1]):
            lup[j] = qs[i]
    lup[255] = qs[-1]

    result_im = lup[im_2].astype(np.float32) / 255
    if is_input_rgb:
        im_yiq[:,:,0] = result_im
        result_im = np.clip(yiq2rgb(im_yiq), 0, 1)

    return [result_im, errors]

def quantization_error(total_hist, qs, zs):
    """
    Calculates the error of the quantization given by zs and qs with respect with image
    given by histogram separated into 256 (with which to look-up p(z) -- i.e., how many
    times intensity z appears in image)
    """

    # Non-inclusive
    error = 0
    for i in range(len(zs) - 1):
        error += np.dot(
            (np.arange(zs[i], zs[i + 1]) - qs[i]) ** 2,
            total_hist[np.arange(zs[i], zs[i + 1])]
        )

    return error

