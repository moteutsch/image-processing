import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], '../project1'))

import src.helper as h
import src.sol1 as sol1
import sol2 as sol2
import numpy as np
import matplotlib.pyplot as plt

image_gray = sol1.read_image(sys.argv[2], sol1.REPRESENTATION_GRAYSCALE)
if sys.argv[1] == 'fourier_der':
    res = sol2.fourier_der(image_gray)
    h.imsave(sys.argv[3], res)
    plt.figure()
    plt.imshow(res, cmap = plt.get_cmap('gray'))
    plt.show()

    res2 = sol2.conv_der(image_gray)
    plt.figure()
    plt.imshow(res2, cmap = plt.get_cmap('gray'))
    plt.show()

    #h.imsave(sys.argv[3], sol2.conv_der(image_gray))
elif sys.argv[1] == 'conv_der':
    h.imsave(sys.argv[3], sol2.conv_der(image_gray))
elif sys.argv[1] == 'blur_spatial':
    h.imsave(sys.argv[3], sol2.blur_spatial(image_gray, int(sys.argv[4])))
elif sys.argv[1] == 'blur_fourier':
    h.imsave(sys.argv[3], sol2.blur_fourier(image_gray, int(sys.argv[4])))
elif sys.argv[1] == 'dft_idft':
    result = sol2.IDFT2(sol2.DFT2(image_gray)).astype(np.float32)
    print(result)
    print(result.dtype)
    h.imsave(sys.argv[3], result)
elif sys.argv[1] == 'compare_dft':
    print(sol2.DFT2(image_gray))
    print(np.fft.fft2(image_gray))
    print('DFT close?')
    print(sol2.DFT2(image_gray) - np.fft.fft2(image_gray))
    print(np.allclose(sol2.DFT2(image_gray), np.fft.fft2(image_gray)))
    print('IDFT of DFT close?')
    print(sol2.IDFT2(sol2.DFT2(image_gray)) - np.fft.ifft2(np.fft.fft2(image_gray)))
    print(np.allclose(sol2.IDFT2(sol2.DFT2(image_gray)), np.fft.ifft2(np.fft.fft2(image_gray))))
else:
    print('Invalid command...')
