import numpy as np
from scipy.signal import convolve2d


# From sol1.py. Using "from sol1 import *" or any other method of
# importing the file failed the presubmit despite you saying the it is allowed.

from scipy.misc import imread, imshow, imsave
from skimage.color import rgb2gray

REPRESENTATION_GRAYSCALE = 1
REPRESENTATION_RGB = 2

REPRESENTATIONS = [REPRESENTATION_GRAYSCALE, REPRESENTATION_RGB]

def _guard_representation(representation):
    if representation not in REPRESENTATIONS:
        raise ValueError


def read_image(filename, representation):
    """
    Reads image from file into "representation" and returns it as image
    np.ndarray of type np.float32 with intensities in [0, 1]
    """

    _guard_representation(representation)
    if not isinstance(filename, str):
        print(str(filename), type(filename))
        raise ValueError

    im = imread(filename)
    if representation == REPRESENTATION_GRAYSCALE:
        return rgb2gray(im)
    return im.astype(np.float32) / 255


### START OF sol2.py

def DFT_change_of_basis_matrix(N):
    """
    Returns a change-of-basis matrix for converting to frequency domain. Depends
    on size of image.
    """
    return np.exp(
        np.dot(np.arange(N).reshape(N, 1), np.arange(N).reshape(N, 1).T)
            * ((-2 * np.pi * 1j) / N)
    )


def IDFT_change_of_basis_matrix(N):
    """
    See DFT_change_of_basis_matrix. (Inverse matrix.)
    """
    return np.linalg.inv(DFT_change_of_basis_matrix(N))


def DFT(signal):
    """
    :param signal (array dtype float32 (N,1))
    :return fourier_signal (array dtype complex128 (N,1))
    """

    N = signal.shape[0]
    return np.dot(DFT_change_of_basis_matrix(N), signal)


def IDFT(fourier_signal):

    N = fourier_signal.shape[0]
    return np.dot(IDFT_change_of_basis_matrix(N), fourier_signal)


def DFT2(image):
    """
    Applies DFT to columns and then to rows (of result).
    """
    return DFT(DFT(image).T).T


def IDFT2(image):
    return IDFT(IDFT(image).T).T


def conv_der(image):
    """
    Calculates image derivative magnitude using convolution with [1, 0, -1] (and transposed)
    """
    return np.sqrt(
        (convolve2d(image, np.array([[1, 0, -1]]), mode='same') ** 2)
        + (convolve2d(image, np.array([[1, 0, -1]]).T, mode='same') ** 2)
    )


def fourier_der(image):
    """
    Calculates image derivative magnitude in fourier space using method learned in class.
    """

    fourier = DFT2(image)
    der_x_mask = (np.arange(image.shape[1]) - np.ceil(image.shape[1] / 2))  .reshape((1, image.shape[1]))
    der_y_mask = (np.arange(image.shape[0]) - np.ceil(image.shape[0] / 2))  .reshape((image.shape[0], 1))

    dx_fourier = np.multiply(np.fft.fftshift(fourier), der_x_mask)
    dy_fourier = np.multiply(np.fft.fftshift(fourier), der_y_mask)

    dx = ((2 * np.pi * 1j) / image.size) * IDFT2(np.fft.ifftshift(dx_fourier))
    dy = ((2 * np.pi * 1j) / image.size) * IDFT2(np.fft.ifftshift(dy_fourier))

    return np.sqrt(
        np.abs(dx) ** 2
        + np.abs(dy) ** 2
    )


def make_gaussian_kernel(size):
    """
    Make an "size"-by-"size" gaussian approximation kernel using binomial coefficients
    """
    k = np.array([1, 1])
    while k.size < size:
        k = np.convolve(k, np.array([1, 1]))

    k = k.reshape((k.size, 1))

    gaussian = convolve2d(k, k.T)
    return gaussian / gaussian.sum()


def make_padded_gaussian_kernel(kernel_size, x, y):
    """
    Makes a gaussian kernel padded to shape (x, y) with zeros, and shifted so that
    the center is at (0, 0)
    """

    gaussian = make_gaussian_kernel(kernel_size)

    left = int(np.floor(x - kernel_size + 1) / 2)
    right = int(np.floor((x - kernel_size) / 2))
    top = int(np.floor((y - kernel_size + 1) / 2))
    bottom = int(np.floor((y - kernel_size) / 2))

    return np.pad(gaussian, ((left, right), (top, bottom)), mode='constant', constant_values=0)


def blur_spatial(im, kernel_size):
    """
    Blur image in spatial domain by convolution with gaussian kernel
    """
    return convolve2d(im, make_gaussian_kernel(kernel_size), mode='same')


def blur_fourier(im, kernel_size):
    """
    Blur image in frequency domain (with gaussian kernel)
    """
    return np.real(IDFT2(
        np.multiply(DFT2(im), DFT2(make_padded_gaussian_kernel(kernel_size, im.shape[0], im.shape[1])))
    )).astype(np.float32)


def normalize_fourier(fourier):
    """
    Normalization of fourier for visualization as mentioned in "Tips: Fourier display" section
    """
    return np.log(1 + np.abs(fourier))


