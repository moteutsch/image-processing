import src.sol1 as sol1

import os

# For numerical/array processing
import numpy as np

# For plotting
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

import scipy
#from scipy.misc import imread, imshow, imsave, toimage
from scipy.misc import imread, imshow, toimage

# RGB 2 gray
from skimage.color import rgb2gray

def test():
    #display_histogram_equalize_unequalized()
    display_histogram_equalize('./Image51.jpg')

def save_path(name, color, type):
    return './out/' + name + '/' + color + '/' + type + '.png'

def imsave(path, arr):
    toimage(arr, cmin=0.0, cmax=1.0).save(path)

def save_image(name, n_quant_images=60):

    try:
        print('Making directories: "./out/' + name + '/gray" and /rgb')
        os.mkdir('./out/' + name)
        os.mkdir('./out/' + name + '/gray')
        os.mkdir('./out/' + name + '/rgb')
    except OSError:
        print('Already exists...')
        pass

    im_path = './' + name + '.jpg'

    image_gray = sol1.read_image(im_path, sol1.REPRESENTATION_GRAYSCALE)
    image_rgb = sol1.read_image(im_path, sol1.REPRESENTATION_RGB)

    imsave(save_path(name, 'gray', 'original'), image_gray)
    imsave(save_path(name, 'rgb', 'original'), image_rgb)

    display_histogram(image_gray)
    plt.savefig(save_path(name, 'gray', 'orig-hist'), bbox_inches='tight')

    plt.close()
    plt.figure()

    display_histogram(image_rgb)
    plt.savefig(save_path(name, 'rgb', 'orig-hist'), bbox_inches='tight')

    plt.close()
    plt.figure()

    equalized_gray = sol1.histogram_equalize(image_gray)[0]

    imsave(save_path(name, 'gray', 'equalized'), equalized_gray)

    equalized_rgb = sol1.histogram_equalize(image_rgb)[0]
    imsave(save_path(name, 'rgb', 'equalized'), equalized_rgb)

    gray_quant_errors = []

    for i in range(2, n_quant_images):
        #try:
        [quantized_eq_im_gray, errs] = sol1.quantize(equalized_gray, i, 10)
        imsave(save_path(name, 'gray', 'equalized-quantized-' + str(i)), quantized_eq_im_gray)

        plt.plot(np.arange(len(errs)) + 1, errs, 'b-', label="Errors for '" + str(i) + "' quantization after iterations")
        plt.savefig(save_path(name, 'gray', 'equalized-quantized-' + str(i) + '-errors'), bbox_inches='tight')
        plt.close()
        plt.figure()

        [quantized_eq_im_rgb, errs] = sol1.quantize(equalized_rgb, i, 10)
        imsave(save_path(name, 'rgb', 'equalized-quantized-' + str(i)), quantized_eq_im_rgb)

        plt.plot(np.arange(len(errs)) + 1, errs, 'b-', label="Errors for '" + str(i) + "' quantization after iterations")
        plt.savefig(save_path(name, 'rgb', 'equalized-quantized-' + str(i) + '-errors'), bbox_inches='tight')
        plt.close()
        plt.figure()

        [quantized_im_gray, errs] = sol1.quantize(image_gray, i, 10)
        imsave(save_path(name, 'gray', 'quantized-' + str(i)), quantized_im_gray)

        plt.plot(np.arange(len(errs)) + 1, errs, 'b-', label="Errors for '" + str(i) + "' quantization after iterations")
        plt.savefig(save_path(name, 'gray', 'quantized-' + str(i) + '-errors'), bbox_inches='tight')
        plt.close()
        plt.figure()

        [quantized_im_rgb, errs] = sol1.quantize(image_rgb, i, 10)
        imsave(save_path(name, 'rgb', 'quantized-' + str(i)), quantized_im_rgb)

        plt.plot(np.arange(len(errs)) + 1, errs, 'b-', label="Errors for '" + str(i) + "' quantization after iterations")
        plt.savefig(save_path(name, 'rgb', 'quantized-' + str(i) + '-errors'), bbox_inches='tight')
        plt.close()
        plt.figure()

        #except:
            #print('Error creating quantized image with "' + str(i) + '" colors. Stopping...')
            #break

    display_histogram(equalized_gray)
    plt.savefig(save_path(name, 'gray', 'equalized-hist'), bbox_inches='tight')

    plt.close()
    plt.figure()

    display_histogram(equalized_rgb)
    plt.savefig(save_path(name, 'rgb', 'equalized-hist'), bbox_inches='tight')

    plt.close()
    plt.figure()


def display_jerusalem():
    sol1.imdisplay('./presubmission/external/jerusalem.jpg', sol1.REPRESENTATION_GRAYSCALE)

def display_monkey():
    sol1.imdisplay('./presubmission/external/monkey.jpg', sol1.REPRESENTATION_RGB)

def display_quantize_jerusalem(n_colors, n_iters):
    im = sol1.read_image('./presubmission/external/jerusalem.jpg', sol1.REPRESENTATION_GRAYSCALE)
    [qim, err] = sol1.quantize(im, n_colors, n_iters)
    sol1.display_image_array(qim, sol1.REPRESENTATION_GRAYSCALE)

def display_quantize_monkey(n_colors, n_iters):
    im = sol1.read_image('./presubmission/external/monkey.jpg', sol1.REPRESENTATION_RGB)
    [qim, err] = sol1.quantize(im, n_colors, n_iters)
    sol1.display_image_array(qim, sol1.REPRESENTATION_RGB)

def display_histogram_equalize(filepath):
    im = sol1.read_image(filepath, sol1.REPRESENTATION_GRAYSCALE)
    # Display regular histogram
    display_histogram(im)
    input("Display next")

    # Display image
    sol1.display_image_array(im, sol1.REPRESENTATION_GRAYSCALE)
    input("Display next")

    im = sol1.read_image(filepath, sol1.REPRESENTATION_GRAYSCALE)
    # Display histogram after quantization
    equalized = sol1.histogram_equalize(im)[0]

    display_histogram(equalized)
    input("Display next")

    # Display image
    sol1.display_image_array(equalized, sol1.REPRESENTATION_GRAYSCALE)
    input("Display next")

def display_histogram_equalize_jerusalem():
    display_histogram_equalize('./presubmission/external/jerusalem.jpg')

def display_histogram_equalize_unequalized():
    display_histogram_equalize('./Unequalized_Hawkes_Bay_NZ.jpg')

def display_histogram(im):
    is_input_yiq = not sol1.is_image_grayscale(im)
    if is_input_yiq:
        im_yiq = sol1.rgb2yiq(im)
        im_to_change = im_yiq[:,:,0]
    else:
        im_to_change = im

    plt.hist(im_to_change)
