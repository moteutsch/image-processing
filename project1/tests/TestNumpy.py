import unittest
import numpy as np
import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], '..'))

import src.sol1

EPSILON = 1e-10

class TestNumpy(unittest.TestCase):

    def test_ndim_of_array_is_size_of_dimensions_tuple(self):
        arr = np.array([[1, 0], [3, 8]])
        self.assertEqual(len(arr.shape), arr.ndim)

    def test_yiq2rgb_with_inverse_is_identity(self):
        im = src.sol1.read_image('../presubmission/external/jerusalem.jpg', 2)
        one = src.sol1.yiq2rgb(src.sol1.rgb2yiq(im))
        two = src.sol1.rgb2yiq(src.sol1.yiq2rgb(im))

        np.testing.assert_allclose(im, one, atol=1e-10)
        np.testing.assert_allclose(im, two, atol=1e-10)

    def test_pointwise_subtraction_gives_same_as_matrix_subtraction(self):

        im = src.sol1.read_image('../presubmission/external/jerusalem.jpg', 2)

        self.assertEqual(
            np.subtract(im, src.sol1.yiq2rgb(src.sol1.rgb2yiq(im)))[40,40,2],
            im[40,40,2] - src.sol1.yiq2rgb(src.sol1.rgb2yiq(im))[40,40,2]
        )

    def test_equalize_on_evenly_distributed_grayscale_image_is_identity_up_to_epsilon(self):
        im_gray = np.linspace(0, 1, 2**8).reshape(2**4, 2**4)
        print(type(im_gray))
        print(im_gray.shape, src.sol1.histogram_equalize(im_gray)[0].shape)
        [eq_im, hist_orig, hist_eq] = src.sol1.histogram_equalize(im_gray)
        np.testing.assert_allclose(im_gray, eq_im, atol=1e-2, rtol=0)

    def make_yiq_im_with_linear_y_zero_iq(self, size):
        im_tmp = np.empty(((size**2)*3), np.float32)
        lin = np.linspace(0, 1, size**2)
        for i in range(size**2):
            im_tmp[3*i] = lin[i]
            im_tmp[3*i + 1] = im_tmp[3*i + 2] = 0.5

        return im_tmp.reshape(size, size, 3)


    def test_equalize_on_evenly_distributed_y_channel_of_yiq_image_is_identity_up_to_epsilon(self):
        im_yiq = self.make_yiq_im_with_linear_y_zero_iq(2**4)
        print('test', im_yiq.shape)
        im_rgb = src.sol1.yiq2rgb(im_yiq)

        [im_eq, orig_hist, new_hist] = src.sol1.histogram_equalize(im_rgb)
        print('test2', im_eq.shape)
        new_yiq = src.sol1.rgb2yiq(im_eq)

        np.testing.assert_allclose(new_yiq, im_yiq, atol=1e-2, rtol=0)

    def test_n_intensity_quantization_of_n_intensity_grayscale_image_is_identity(self):
        return

        im_gray = np.empty((2**4, 2**4), np.float32)
        im_gray[:2**2,:] = 0.7
        im_gray[2**2:,:] = 0.3

        [res_im, err] = src.sol1.quantize(im_gray, 2, 5)

        print(im_gray, res_im)
        np.testing.assert_allclose(im_gray, res_im, atol=0.1, rtol=0)
        for i in err:
            self.assertTrue(i < EPSILON)

    def test_n_intensity_quantization_of_n_intensity_rgb_image_is_identity(self):
        return

        im_yiq = np.zeros((2**4, 2**4, 3), np.float32)
        im_yiq[:,:,:] = 0.5
        im_yiq[:2**2,:, 0] = 0.7
        im_yiq[2**2:,:, 0] = 0.3

        [res_im, err] = src.sol1.quantize(src.sol1.rgb2yiq(im_yiq), 2, 5)
        res_im_yiq = src.sol1.rgb2yiq(res_im)

        np.testing.assert_allclose(res_im_yiq, res_im, atol=1e-5, rtol=0)
        for i in err:
            self.assertTrue(i < EPSILON)

    def test_cumsum_of_hist_of_any_image_is_almost_linear(self):
        pass
        '''
        for i in range(10):
            im = np.random.RandomState(i).normal(loc=0.5, scale=0.12, size=(10,10))
            hist, bin_edges = np.histogram(src.sol1.histogram_equalize(im_gray))
            np.cumsum(hist)
        '''
        pass

    def test_hist_error(self):
        self.assertTrue(self.hist_error([0.3, 0.4, 0.5], [0.51, 0.29, 0.39]) < 0.05)

    def hist_error(self, arr1, arr2):
        errors = []
        for i in [-1, 0, 1]:
            errors.append(np.sum((np.roll(arr1, i) - arr2) ** 2))
        return min(errors)


if __name__ == '__main__':
    unittest.main()
